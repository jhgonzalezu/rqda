## JhonattanGonzalez


## Packages

import tkinter as tk
from tkinter import *
from tkinter import filedialog

from PyPDF2 import PdfFileReader


## Programa

root = tk.Tk()

etiqueta = tk.Label(root, text="Conversor de archivos a TXT")
etiqueta.pack()
root.title("Conversor a TXT")
root.config(width=400, height=300)

global name_file



def abrirArchivo():
    global archivo
    global tip_text
    archivo = filedialog.askopenfilename(title="Elegir archivo][", initialdir="/home",filetypes=(("Archivos PDF","*.pdf"),("Archivos JPG","*.jpg"),("Archivos WORD","*.docx"),("Archivos open doc","*.odt")))
    print(archivo)
    s = archivo
    pdf  = '.pdf'
    jpg  = '.jpg'
    odt  = '.odt'
    docx = '.docx'
    if s.find(pdf) != -1 :
        tip_text = 1 # 1 -> pdf
        print(s.find(pdf))
        print('pdf')
    elif s.find(jpg) != -1:
        tip_text = 2 # 2 -> jpg
        print(s.find(jpg))
        print('jpg')
    elif s.find(odt) != -1:
        tip_text = 3 # 3 -> odt
        print(s.find(odt))
        print('odt')
    elif s.find(docx) != -1:
        tip_text = 4 # 4 -> docx
        print(s.find(docx))    
        print('doc')
    elif (s.find(pdf) & s.find(jpg) & s.find(odt) & s.find(docx))==-1:
        tip_text = 0 # 0 -> not file
        print("Archivo equivocado o no seleccionado")

    print(tip_text)
#    print(name_file)
 
def pdf_converted():
    print('si entra')
    name=str(name_file.get())
    print(name)
    name_file.delete(0, END)
    #name_file = tk.Entry(root).pack()
    #name=name_file.get()
    pdf = PdfFileReader(str(archivo))
    x=pdf.numPages
    n=0

    while(n<x):
        print(n)
        print(name_file)
        print(name)
        pageobj=pdf.getPage(n)
        if (pageobj.extractText()):
            text=pageobj.extractText()
            file=open(name+'.txt',"a",encoding='utf-8')
            file.write(text)
        n=n+1

    pdf=''
    
    #archivo=''
    

    

def conversor():
    if tip_text==1:
        pdf_converted()
        print(tip_text)
    elif tip_text==2:
        jpg_converted()
    elif tip_text==3:
        odt_converted()
    elif tip_text==4:
        docx_converted()


def docx_converted():
    print('docx')
def jpg_converted():
    print('jpg')
def odt_converted():
    print('odt')



tk.Label(root, text="Nombre para archivo:").pack()
name_file = tk.Entry(root)
name_file.pack()

Button(root, text="Elegir archivo",command=abrirArchivo).pack()
Button(root, text="Convertir archivo",command=conversor).pack()

etiqueta.pack()
root.mainloop()
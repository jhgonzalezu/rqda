## JhonattanGonzalez


## Packages

import tkinter as tk
from tkinter import *
from tkinter import filedialog

from PyPDF2 import PdfFileReader


## Programa

root = tk.Tk()

etiqueta = tk.Label(root, text="Conversor de PDF a TXT")
etiqueta.pack()
root.title("Conversor a TXT")
root.config(width=400, height=300)

def abrirArchivo():
    global archivo
    archivo = filedialog.askopenfilename(title="Elegir archivo ][", initialdir="C:/",filetypes=(("Archivos PDF","*.pdf"),("Archivos JPG","*.jpg"),("Archivos WORD","*.docx"),("Archivos open doc","*.odt")))

def conversor():
    pdf = PdfFileReader(str(archivo))
    x=pdf.numPages
    n=0
    open(r"PDFconversor\\Documento conpes_3463.txt","a")
    while(n<x):
        print(n)
        pageobj=pdf.getPage(n)
        text=pageobj.extractText()
        file=open("PDFconversor\\Documento conpes_3463.txt.txt","a",encoding='utf-8')
        file.write(text)
        n=n+1

Button(root, text="Elegir archivo",command=abrirArchivo).pack()
Button(root, text="Convertir archivo",command=conversor).pack()
etiqueta.pack()
root.mainloop()

from calendar import c
import dash
import dash_bootstrap_components as dbc
import sqlite3

from dash import Input, Output, dcc, html, State
##from importlib_metadata import files
from numpy import False_

import pandas as pd
import pandas.io.sql as sqlio
import plotly.express as px

from datetime import date 

from random import randint

import dash_uploader as du

import os
import shutil

##import openpyxl

##
conexion = sqlite3.connect('')

global accept
global du_file_name,du_file_address,du_file_remove
du_file_remove='./BD/not'
du_file_name = 'NA'
du_file_name_RQDA = './BD/'+'BD_Cuali_Evaluacion PNGIRH_V4.rqda'

app = dash.Dash(
    __name__,
    external_stylesheets=[
        dbc.themes.BOOTSTRAP
    ]
)
server = app.server

UPLOAD_FOLDER_ROOT = ".\BD"
du.configure_upload(app, UPLOAD_FOLDER_ROOT)


## Componentes generales
navbar = dbc.Navbar(
    dbc.Row(
        [
            dbc.Col(width=1),
            dbc.Col(width=1,align='center'),
            dbc.Col(width=7),
            dbc.Col(
                dbc.Container(
                    [
                        html.A(
                            dbc.Row(html.Img(src='/assets/logos/logoik.png', width='200px'),),
                            href="https://web.infometrika.com/",
                        ),            
                    ],
                    className="navbar_class"
                ),
                width=3
            ),
            
        ],
    ),
    color="#b6b6b6",
)

content = html.Div(id="page-content", className="body")

info = html.Div(
    dbc.Row(html.P("OHK",className='letterDark'),className='info_tab')
)


## Componentes INICIO
button_content= dbc.Button(
    [
        html.P("Ingresar",className='letter')
    ],
    id='button-content',
    n_clicks=0,
    href='/reportes-rqda-ik-cuali/overview',
    #disabled=False,
    color='danger',
    outline=True
)

upload_BD = du.Upload(
        id='upload-BD',
        max_file_size=1000,  # 1800 Mb
        filetypes=['rqda'],
        text='Seleccione o arrastre el archivo de RQDA aquí!',
    )

insertar_BD = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(width=2),
                dbc.Col(
                    [
                        dbc.Card(
                            [
                                dbc.CardBody(
                                    [
                                        html.H2("Bienvenidos",className='letter'),
                                        html.H1("Administrador de RQDA",className='letterN'),
                                        html.Hr(),
                                        html.H4("Esta es una herramienta creada para complementar el software en base R de análisis cualitativo RQDA",className='letter'),
                                        html.H5("Permite hacer análisis estadísticos de información cualitativa alojada en las bases de datos RQDA que son procesadas por los trabajadores de Infométrika en el área Cualitativa y aquellos que deseen consultar la información especifica con más detalle y filtrando datos necesarios",className='letter'),
                                        html.H5("Para poder acceder, ingrese a continuación la base de datos RQDA",className='letter'),
                                        html.Br(),
                                        dbc.Row(
                                            [
                                                dbc.Col(
                                                    [
                                                        html.Div(id='callback-output'),
                                                        upload_BD,
                                                    ],
                                                    
                                                    #dbc.Input(id="input-BD", placeholder="Ingresa la ubicación del directorio de RQDA...", type="text",className='letter'),
                                                    width=10
                                                ),
                                                dbc.Col(
                                                    [   
                                                        button_content,
                                                    ],
                                                    align='center'
                                                )
                                            ],
                                            align='center'
                                        )
                                    ]
                                )
                            ],
                            color='#F0F0F0'
                        ),
                    ]
                ),
                dbc.Col(
                    [
                        dbc.Row(style={'heigth':'60px'}),
                        dbc.Row(html.Div(id='alert-success'))
                    ],
                    width=2
                ),
            ]
        )
        
    ],
)


## Componentes init
informacion_general='Bienvenidos a la herramienta para RQDA, en la siguiente sección se cuentan con varias opciones para poder hacer complemento al uso del software RQDA, con las siguientes opciones:'
basededatos='- Base de datos RQDA: Se hace publicación de todas las tablas alojadas en lenguaje SQLite'
reportes='- Reportes: Permite elegir las tablas para reportes que se requieren en archivos CSV, dando la posibilidad de ver la tabla resultado, y exportarla o copiarla según la necesidad'
analisiscuali='- Análisis cualitativo: Muestra las posibles combinaciones entre datos cualitativos de la base de datos, publicando gráficos de importancia'
conversion='- Conversión txt: Esta opción ayudará en el proceso de codificación, ya que la base de datos RQDA requiere de archivos sin formato de texto, es decir con extensión .txt, podrá subir archivos principalmente de tipo PDF y guardarlos como un archivo TXT'

button_content_out= dbc.Button(
    [
        html.H5("Cerrar BD",className='letterL')
    ],
    id='button-content-out',
    n_clicks=0,
    href='/reportes-rqda-ik-cuali',
    color='light',
    outline=True
)



init = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    dbc.Card(
                        dbc.CardBody(
                            [
                                
                                html.Hr(),
                                dbc.Row(
                                    [
                                        dbc.Col(html.Div(id='card-bd'),width=9),
                                        dbc.Col(button_content_out,align='center')
                                    ]
                                ),
                                html.Hr(),
                                html.P(informacion_general,className='letterLeft'),
                                html.Hr(),
                                html.P(basededatos,className='letterLeft'),
                                html.P(reportes,className='letterLeft'),
                                html.P(analisiscuali,className='letterLeft'),
                                html.P(conversion,className='letterLeft'),
                            ]
                        ),
                        color='#F0F0F0',
                        outline=True,
                    ),
                    width=5
                ),
                dbc.Col(width=1),
                dbc.Col(
                    [
                        dbc.Nav(
                            [
                                dbc.NavLink(
                                    [
                                        html.H3("BASE DE DATOS",className='letter_menu'),
                                        dbc.Row(
                                            [
                                                dbc.Col(width=2),
                                                dbc.Col(dbc.Row(html.Img(src='/assets/iconos/BD.png',width='120px'),align='center'),align='center'),
                                                dbc.Col(width=2)   
                                            ],align='center'
                                        )
                                    ],
                                    href='/reportes-rqda-ik-cuali/overview/BD',
                                ),
                                html.Br(),
                                dbc.NavLink(
                                    [
                                        html.H3("Reportes",className='letter_menu'),
                                        dbc.Row(
                                            [
                                                dbc.Col(width=2),
                                                dbc.Col(dbc.Row(html.Img(src='/assets/iconos/tables.png',width='120px'),align='center'),align='center'),
                                                dbc.Col(width=2)   
                                            ],align='center'
                                        )
                                    ],
                                    href='/reportes-rqda-ik-cuali/overview/Reportes',
                                )
                            ],
                            vertical=True,
                            pills=True,
                            navbar=True,
                        )
                    ],
                    align='center'
                ),
                dbc.Col(
                    [
                        dbc.Nav(
                            [
                                dbc.NavLink(
                                    [
                                        html.H3("Análisis cualitativo",className='letter_menu'),
                                        dbc.Row(
                                            [
                                                dbc.Col(width=2),
                                                dbc.Col(dbc.Row(html.Img(src='/assets/iconos/datacuali.png',width='120px'),align='center'),align='center'),
                                                dbc.Col(width=2)   
                                            ],align='center'
                                        )
                                    ],
                                    href='/reportes-rqda-ik-cuali/overview/analisis-cuali',
                                ),
                                html.Br(),
                                dbc.NavLink(
                                    [
                                        html.H3("Conversión txt",className='letter_menu'),
                                        dbc.Row(
                                            [
                                                dbc.Col(width=2),
                                                dbc.Col(dbc.Row(html.Img(src='/assets/iconos/txt.png',width='120px'),align='center'),align='center'),
                                                dbc.Col(width=2)   
                                            ],align='center'
                                        )
                                    ],
                                    href='/reportes-rqda-ik-cuali/overview/conversion-archivos-txt',
                                )
                            ],
                            vertical=True,
                            pills=True,
                            navbar=True,
                        )
                    ],
                    align='center'
                ),
                dbc.Col(width=1)
            ],
            align='center'
        ),
    ]
)




## Componentes Base de Datos tablas
public_BD = html.Div()


## Componentes Reportes Base de datos
reportes_BD = html.Div(
    [
        dbc.Card(
            dbc.CardBody(
                [
                    html.Br(),
                    html.P("Se realizan los reportes requeridosm en un archivo .xlsx",className='letterLeft'),
                    html.P("Elija los datos del RQDA",className='letterLeft'),
                    html.Br(),
                    dcc.Dropdown(
                        [
                            'Annotation',
                            'Attributes',
                            'Cases',
                            'Files',
                            'Category Files',
                            'Codes',
                            'Category Codes',
                            'Memos',
                            'Journals'
                        ],
                        ['Files','Codes'],
                        multi=True,
                        id='dropdown-analisis'
                    ),
                ]
            )
        ),
        dbc.Card(
            dbc.CardBody(
                [
                    dbc.Row(
                        [
                            dbc.Col(
                                html.P("Reporte General",className='letterLeft'),
                                width=9
                            ),
                            dbc.Col(
                                [
                                    html.Button("Download Excel", id="btn_xlsx"),
                                    dcc.Download(id='dwld'),
                                ],
                                width=3
                            )
                        ]
                    ),
                ]
            )
        )
    ],
)

## Componentes conversion de archivos txt
conversion_txt = html.Div()


## Componentes Análisis estadístico
button_home= dbc.Button(
    children="Menú",
    href='/reportes-rqda-ik-cuali/overview',
    style={ 
        'background':'#BBBBBB',
        'border':'solid 1px #d61425',        
    },
)

content_analisis = html.Div(id="page-content-analisis", className="body")

spin = html.Div(
    dbc.Row(
        dbc.Col(dbc.Spinner(color="#F05454", type="grow"),align='center'),
        align='center'
    )
)

menu_analisis = html.Div(
    [
        dbc.Card(
            [
                dbc.CardHeader(
                    html.H5("Frecuencia de Codificaciones",className='letterLeft')
                ),
                dbc.CardBody(
                    [
                        html.Div("Holi")
                    ]
                ),
            ],
            color='#F0F0F0'
        ),
    ]
)

estadistica_BD = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.Card(
                            dbc.CardBody(
                                [
                                    dbc.Row(button_home,align='center'),
                                    html.Hr(),
                                    html.P("Ingresa los datos alojados en RQDA",className='letterLeft'),
                                    html.Hr(),
                                    dcc.Dropdown(
                                        [
                                            'Annotation',
                                            'Attributes',
                                            'Cases',
                                            'Files',
                                            'Category Files',
                                            'Codes',
                                            'Category Codes',
                                            'Memos',
                                            'Journals'
                                        ],
                                        [
                                            'Annotation',
                                            'Attributes',
                                            'Cases',
                                            'Files',
                                            'Category Files',
                                            'Codes',
                                            'Category Codes',
                                            'Memos',
                                            'Journals'
                                        ],
                                        multi=True,
                                        id='dropdown-analisis'
                                    ),
                                    dbc.Row(
                                        [
                                            dbc.Col(width=5),
                                            dbc.Col(
                                                dbc.Button(
                                                    html.P("Consultar",className='letter'),
                                                    id='button-analisis-consulta',
                                                    color="danger",
                                                    outline=True,
                                                    n_clicks=0
                                                ),
                                                width=3
                                            )
                                        ],
                                        align='end'
                                    ),
                                ]
                            ),
                            color='#F0F0F0'
                        ),
                    ],
                    width=2
                ),
                dbc.Col(
                    content_analisis
                )
            ]
        ),
    ]
)

tabs = html.Div(
    [
        dbc.Tabs(
            [
                dbc.Tab(label="Información General", tab_id="tab-1"),
                dbc.Tab(label="Gráficas", tab_id="tab-2"),
                #dbc.Tab(label="memos"
                #", tab_id="tab-2"),
            ],
            id="tabs",
            active_tab="tab-1",
        ),
        html.Div(id="content"),
    ]
)


#@app.callback(Output("content", "children"), [Input("tabs", "active_tab")])
#def switch_tab(at):
#    if at == "tab-1":
#        return tab1_content
#    elif at == "tab-2":
#        return tab2_content
#    return html.P("This shouldn't ever be displayed...")




# General
modal = dbc.Modal(
            [
                dbc.ModalBody([
                    html.Hr(),
                    "Recuerde al finalizar cerrar la Base de Datos",
                    html.Hr()
                ]),
            ],
            id="modal-backdrop",
            is_open=True,
        ),

app.layout = dbc.Container(
    [
        
        html.Div(
            [
                dcc.Location(id="url",pathname="/reportes-rqda-ik-cuali"),
                navbar,
                content,
                info,
                
            ]
        ), 
        #modal,
    ],
    fluid=True,
)




## CALLBACKS

# Revisar PATH URL
@app.callback(
        Output("page-content", "children"),
        Input("url", "pathname"),
)
def update_url(path):  
    
    if path=='/reportes-rqda-ik-cuali':
        #global du_file_name,du_file_address
        #du_file_name_RQDA='NA'
        #if os.path.exists(du_file_remove):
        #    shutil.rmtree(du_file_remove)
        return insertar_BD,False
    if path=='/reportes-rqda-ik-cuali/overview':
        return init,True
    elif path=='/reportes-rqda-ik-cuali/overview/BD':
        return public_BD,False
    elif path=='/reportes-rqda-ik-cuali/overview/Reportes':
        return reportes_BD,False
    elif path=='/reportes-rqda-ik-cuali/overview/conversion-archivos-txt':
        return conversion_txt,False
    elif path=='/reportes-rqda-ik-cuali/overview/analisis-cuali':
        return estadistica_BD,False


# Subir Base de Datos RQDA
#@du.callback(
#    output=Output('callback-output', 'children'),
#    id='upload-BD',
#)
def get_a_list(filenames):
    global du_file_address,du_file_name,du_file_remove
    name=str(filenames).split(sep='\\')
    #du_file_remove = './BD/'+name[2]
    du_file_remove = './BD/'+'BD_Cuali_Evaluacion PNGIRH_V4.rqda'
    du_file_name = name[-1]
    du_file_name=str(du_file_name).split(sep="'")
    du_file_name = du_file_name[0]
    du_file_address = filenames
    return html.Div([html.P(du_file_name)])


# Revisar si hay Base de Datos
@app.callback(
    [
        #Output('button-content','disabled'),
        Output('alert-success','children'),
    ],
    Input("callback-output", "children")
)
def pathname_update(value):
    alert = dbc.Alert(
        [
            html.Img(src='/assets/iconos/alert.png',height='20px'),
            html.P("No hay base de datos de RQDA",className='letter'),
        ],
        color="danger",
        className="d-flex align-items-center",
    ),
    #if du_file_name != '':
    print(du_file_name_RQDA)
    if du_file_name_RQDA != '':
        print(du_file_name_RQDA)
        
        name = du_file_name_RQDA
        success = [dbc.Alert(
            [
                html.Img(src='/assets/iconos/success.png',height='20px'),
                html.P(f"Base de datos {name}",className='letter'),
            ],
            color="success",
            className="d-flex align-items-center",
        )]
        if '.rqda' in du_file_name_RQDA:
            global file_address
            #file_address = str(du_file_address).split("'")
            
            #conexion=sqlite3.connect(file_address[1])
            conexion=sqlite3.connect(du_file_name_RQDA)
            dis = False
            card= success 
        else:
            dis = True
            card= alert
    else:
        dis = True
        card= alert
    
    
    #return dis, card
    return card



# Overview
@app.callback(
    Output('card-bd','children'),
    Input('button-content-out','n_clicks')
)
def BD_update(n):
    card=dbc.Card(
        dbc.CardBody(
            [
                html.H6("Conectado al proyecto RQDA:",className='letterDark'),
                html.Div(du_file_name_RQDA,className='letterDarkC')
            ]
        ),
        color='#F05454'
    ),
    return card


# BD
#@app.callback()
#def BD_update():
        

# analisis estadistico
"""@app.callback(
    [
        Output("content-tw", "children"),
        Output("tw-tw","n_clicks"),
        Output("tw-sen","n_clicks"),
        Output("tw-wc","n_clicks"),
        
    ],
    [
        Input("tabs-tw", "active_tab"),
        Input("tw-tw","n_clicks"),
        Input("tw-sen","n_clicks"),
        Input("tw-wc","n_clicks"),
    ]
)

def switch_tab(at1,n1,n3,n4):
    if (at1 == "tab-tw-pol")or n1!=0:
        tab_return=tab_tw_pol_content
    if (at1 == "tab-tw-sen")or(n3!=0):
        tab_return= tab_tw_sen_content
    if (at1 == "tab-tw-wc")or(n4!=0):
        tab_return=tab_tw_wc_content
    return tab_return,0,0,0"""

graf = ['#5F4690','#1D6996','#38A6A5','#0F8554','#73AF48','#EDAD08','#E17C05','#CC503E','#94346E','#6F4070','#994E95','#666666']

def colorSelected(h):
    n=randint(0, h)
    for k in range(90):
        graf[k]=()
    return graf
    

@app.callback(
    [
        Output('page-content-analisis','children'),
        Output('button-analisis-consulta','n_clicks')
    ],
    [
        Input('button-analisis-consulta','n_clicks'),
        Input('dropdown-analisis','value'),
    ],
)
def BD_update(value,item):
    if value == 1:
        html_0 = html.Div(
            [html.Hr()]
        )
        tarj_graf=[]
        conexion=sqlite3.connect(du_file_name_RQDA)
        #if 'Annotation' in item:
        #if 'Attributes' in item:
        #if 'Cases' in item:
            #cases = pd.read_sql_query("""Select * from cases""",conexion)
            #cases_merge = pd.read_sql_query("""Select * from caselinkage""",conexion)
        codes = pd.read_sql_query("""Select * from freecode""",conexion)
        coding = pd.read_sql_query("""Select * from coding""",conexion)
        if 'Codes' in item:
            codes = pd.read_sql_query("""Select * from freecode""",conexion)
            coding = pd.read_sql_query("""Select * from coding""",conexion)
            
            codes = pd.concat([codes['name'],codes['memo'],codes['id']],axis=1)
            codes = codes.rename(columns={'id':'cid','name':'name-code'})

            coding = pd.concat([coding['cid'],coding['fid'],coding['seltext'],coding['owner']],axis=1)
            
            ## Frecuencia de codificacion por codigo 

            codes_frec = pd.merge(codes,coding, on='cid', how='outer')
            codes_frec = pd.concat([codes_frec['name-code'],codes_frec['cid']],axis=1)
            codes_frec = codes_frec.groupby(['name-code']).size()
            codes_frec = codes_frec.reset_index()
            codes_frec = codes_frec.rename(columns={0:'frec'})
            codes_frec = codes_frec.sort_values(by='frec')
            fig_frec_codes=px.bar( 
                codes_frec, 
                x="frec",
                y="name-code",
                color="frec",
                orientation='h',
            )
            fig_frec_codes.update_layout(
                                xaxis_title = "Frecuencia", 
                                yaxis_title = 'Códigos',
                                )


            if 'Category Codes' in item:
                category_code = pd.read_sql_query("""Select * from codecat""",conexion)
                category_code_merge = pd.read_sql_query("""Select * from treecode""",conexion)

                category_code = pd.concat([category_code['name'],category_code['memo'],category_code['catid']],axis=1)
                category_code = category_code.rename(columns={'catid':'Ccatid','name':'name-catcod'})
                
                category_code_merge = pd.concat([category_code_merge['cid'],category_code_merge['catid']],axis=1)
                category_code_merge = category_code_merge.rename(columns={'catid':'Ccatid'})

                ## Frecuencia de codificacion por categoria de codigo
                catcodes_frec = pd.merge(codes,coding, on='cid', how='outer')

                catcodes_c = pd.merge(category_code_merge,category_code, on='Ccatid', how='outer')
                catcodes_frec = pd.merge(catcodes_c,catcodes_frec, on='cid', how='outer')

                catcodes_frec_A = pd.concat([catcodes_frec['name-catcod'],catcodes_frec['name-code']],axis=1)
                catcodes_frec = catcodes_frec_A.groupby(['name-catcod']).size()
                catcodes_frec = catcodes_frec.reset_index()
                catcodes_frec = catcodes_frec.rename(columns={0:'frec'})
                catcodes_frec = catcodes_frec.sort_values(by='frec')
                fig_frec_catcod=px.bar( 
                    catcodes_frec, 
                    x="frec",
                    y="name-catcod",
                    color="frec",
                    orientation='h',
                    color_discrete_sequence= px.colors.sequential.Aggrnyl
                )
                fig_frec_catcod.update_layout(
                                    xaxis_title = "Frecuencia", 
                                    yaxis_title = 'Categoría de Códigos',
                                    )

                ## Frecuencia de codificacion por categoria de codigo y código
                catcodes_cod_frec = catcodes_frec_A.groupby(['name-code','name-catcod']).size()
                catcodes_cod_frec = catcodes_cod_frec.reset_index()
                catcodes_cod_frec = catcodes_cod_frec.rename(columns={0:'frec'})
                catcodes_cod_frec = catcodes_cod_frec.sort_values(by='name-code')
                print(graf)
                
                fig_frec_catcod_cod=px.bar( 
                    catcodes_cod_frec, 
                    x="frec",
                    y="name-code",
                    #y="name-code",
                    #color="name-catcod",
                    color="name-catcod",
                    orientation='h',
                    color_discrete_sequence= px.colors.qualitative.Safe,
                    #hover_name="name-code"
                )
                #title = "Frecuencia de codificación por categoría de código",
                fig_frec_catcod_cod.update_layout(
                                    xaxis_title = "Frecuencia", 
                                    yaxis_title = 'Códigos',

                                    )
        if 'Files' in item:  
            files = pd.read_sql_query("""Select * from source""",conexion)
            
            files = pd.concat([files['name'],files['memo'],files['id']],axis=1)
            files = files.rename(columns={'id':'fid','name':'name-file'})
            
            ## Frecuencia de codificacion por archivo

            files_frec = pd.merge(files,coding, on='fid', how='outer')
            files_frec = pd.concat([files_frec['name-file'],files_frec['fid']],axis=1)
            files_frec = files_frec.groupby(['name-file']).size()
            files_frec = files_frec.reset_index()
            files_frec = files_frec.rename(columns={0:'frec'})
            files_frec = files_frec.sort_values(by='frec')
            fig_frec_files=px.bar( 
                files_frec, 
                x="frec",
                y="name-file",
                color="frec",
                orientation='h',
            )
            fig_frec_files.update_layout(
                                xaxis_title = "Frecuencia", 
                                yaxis_title = 'Archivos',
                                )

            if 'Category Files' in item:
                category_file = pd.read_sql_query("""Select * from filecat""",conexion)
                category_file_merge = pd.read_sql_query("""Select * from treefile""",conexion)

                category_file = pd.concat([category_file['name'],category_file['memo'],category_file['catid']],axis=1)
                category_file = category_file.rename(columns={'catid':'Fcatid','name':'name-catfile'})

                category_file_merge = pd.concat([category_file_merge['fid'],category_file_merge['catid']],axis=1)
                category_file_merge = category_file_merge.rename(columns={'catid':'Fcatid'})

                ## Frecuencia de codificacion por categoria de archivo
                
                catfiles_frec = pd.merge(files,coding, on='fid', how='outer')

                catfiles_c = pd.merge(category_file_merge,category_file, on='Fcatid', how='outer')
                catfiles_frec = pd.merge(catfiles_c,catfiles_frec, on='fid', how='outer')

                catfiles_frec_A = pd.concat([catfiles_frec['name-catfile'],catfiles_frec['name-file']],axis=1)
                catfiles_frec = catfiles_frec_A.groupby(['name-catfile']).size()
                catfiles_frec = catfiles_frec.reset_index()
                catfiles_frec = catfiles_frec.rename(columns={0:'frec'})
                catfiles_frec = catfiles_frec.sort_values(by='frec')
                fig_frec_catfiles=px.bar( 
                    catfiles_frec, 
                    x="frec",
                    y="name-catfile",
                    color="frec",
                    orientation='h',
                )
                fig_frec_catfiles.update_layout(
                                    xaxis_title = "Frecuencia", 
                                    yaxis_title = 'Categoría de Archivo',
                                    )

                #tarj_graf[1]=fig_frec_catfiles

                ## Frecuencia de codificacion por categoria de codigo y código
                catfiles_cod_frec = catfiles_frec_A.groupby(['name-file','name-catfile']).size()
                catfiles_cod_frec = catfiles_cod_frec.reset_index()
                catfiles_cod_frec = catfiles_cod_frec.rename(columns={0:'frec'})
                catfiles_cod_frec = catfiles_cod_frec.sort_values(by='name-file')
                
                fig_frec_catfiles_files=px.bar( 
                    catfiles_cod_frec, 
                    x="frec",
                    y="name-file",
                    #y="name-code",
                    #color="name-catcod",
                    color="name-catfile",
                    orientation='h',
                    color_discrete_sequence= px.colors.qualitative.Safe,
                    #hover_name="name-code"
                )
                #title = "Frecuencia de codificación por categoría de código",
                fig_frec_catfiles_files.update_layout(
                                    xaxis_title = "Frecuencia", 
                                    yaxis_title = 'Archivos',
                                    )
                #tarj_graf[2] = fig_frec_catfiles
            #else :
                #tarj_graf[1] = html_0
                #tarj_graf[2] = html_0
        #else: 
            #tarj_graf[0] = html_0
            #tarj_graf[1] = html_0
            #tarj_graf[2] = html_0
        


        if 'Memos' in item:
            memo = pd.read_sql_query("""Select * from coding""",conexion)

        if 'Journals' in item:
            journals = pd.read_sql_query("""Select * from journal""",conexion)
        
        info = pd.read_sql_query("""Select * from project""",conexion)

        figa = html.Div(
            [
                dbc.Row(
                    [
                    dcc.Graph(
                        figure=fig_frec_files,
                    ),
                    spin,],
                    style={'height':'1200px'}
                ),
                dbc.Row(
                    [
                    dcc.Graph(
                        figure=fig_frec_codes,
                    ),
                    spin,],
                    style={'height':'1200px'}
                ),
                html.Br(),
                dbc.Row(
                    dcc.Graph(
                        figure=fig_frec_catcod,
                    ),
                    style={'height':'550px'}
                ),
                html.Br(),
                dbc.Row(
                    dcc.Graph(
                        figure=fig_frec_catcod_cod,
                    ),
                    style={'height':'800px'}
                ),
                html.Br(),
                


                dbc.Row(
                    dcc.Graph(
                        figure=fig_frec_files,
                    ),
                    style={'height':'900px'}
                ),
                html.Br(),
                dbc.Row(
                    dcc.Graph(
                        figure=fig_frec_catfiles,
                    ),
                    style={'height':'550px'}
                ),
                html.Br(),
                dbc.Row(
                    dcc.Graph(
                        figure=fig_frec_catfiles_files,
                    ),
                    style={'height':'800px'}
                ),
            ],

        )
        #tabla = dbc.Table.from_dataframe(coding)
        return figa,0
    elif value == 0:
        return spin,0
    

# reportes
#@app.callback()
#def BD_update():





        cases = pd.read_sql_query("""Select * from cases""",conexion)

@app.callback(
    Output("dwld", "data"),
    Input("btn_xlsx", "n_clicks"),
    prevent_initial_call=True,
)
def func(n_clicks):
    conexion=sqlite3.connect(du_file_name_RQDA)
    df = pd.read_sql_query("""
                    SELECT 	source.name [Nombre de archivo],filecat.name [Categoría de archivo], codecat.name [Categoría de código],freecode.name [Código],coding.seltext [Texto citado por código]--
                    FROM 	source,freecode,coding,codecat, treecode, treefile,filecat--,
                    WHERE coding.status LIKE 1
                    AND treefile.status LIKE 1
                    AND treefile.fid = source.id
                    AND treefile.catid = filecat.catid
                    AND		coding.cid = freecode.id
                    AND 	coding.fid = source.id
                    AND 	freecode.id= treecode.cid
                    AND 	treecode.catid = codecat.catid
                    """,conexion)
    return dcc.send_data_frame(df.to_excel, "mydf.xlsx", sheet_name="REPORTE BD")


# txt
#@app.callback()
#def BD_update():


## Run 
if __name__ == "__main__":
    app.run_server(port=7887)






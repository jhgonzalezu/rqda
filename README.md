# RQDA Instructional 0.3-2
#### 1.0 Tool for the analysis of qualitative information 

By Infométrika S.A.S

## 1 Introducción

This instruction manual has the purpose of restructuring, reorganizing and retaking all information available on internet and r-cran repositories around RQDA, a free software environment for the cualitative analysis, where the textual data and investigative reports that together with R environment, complement an interface that allows the user to interact with qualitative and quantitative data, it also has several report formats as needed, in addition, the necessary software installation for it's use is explained.

## 2 Software instalation

The installation of all applications, needed extensions and libraries for working with R and R-base Qualitative Data Analysis (RQDA) is explain here.

### 2.1 R environment

To start this proccess you need to know the environment and language R, a free software with capacity to manage graphs and statistical computing in differents operatives systems (Windows, Linux and macOS), providing a wide variety of estatistical techniques such as cuantitatives and qualitatives clasifications, linear and non-linear regressions, time series, groupings, among others (The Comprehensive R Archive Network, n.d.) with support of various forms of reports per proyect, using guides, instructives, graphics, modelings and more, to get to the RQDA qualitative analysis tool.

### 2.1.1 R Download

To Download the R environment, go to the [r-cran](https://cran.r-project.org/ "r-cran repository") repository (The Comprehensive R Archive Network) where are all compiled binary distributions and contributory packages to complement them, in this instructive, we will choose the standar mirror [0-cloud](https://cloud.r-project.org/"cloud.r-project.org").

##### Windows

Download R 4.0.5 for windows and run the installer according to system requirements.

##### Linux
In the terminal run the following code:
```terminal
$ sudo apt-get upgrade
$ sudo apt-get update 
$ sudo apt-get install r-base r-base-dev
$ # Start R console
$ R
```

##### macOS
Download R-4.0.5.pkg for macOS and run the installer according to system requirements.

### 2.1.2 Coding language R

Run in R console this command:
```R
# Returns the current location of the working directory
getwd()
# This command modifies the location of the working directory
setwd("\\home\\[file]") # Linux

```
### 2.2 Rtools

Only for Windows, the step one install [msys2](https://www.msys2.org/"MSYS2") which makes it easy to compile and maintain some R libraries and packages. The step two, install [rtools40](https://cran.r-project.org/bin/windows/Rtools/"Rtools 40") tool.

### 2.3 RStudio

Is an open source or commercial integrated development environment (IDE) for encoding R. The installers are in [RStudio](https://www.rstudio.com/products/rstudio/download/#download"RStudio").

### 2.4 RQDA installer

Run in R console this command:
```R
# Install devtools packages 
install.packages("devtools")
# then enable the packages
library(usethis)
library(devtools)
```
#### Install GTK+ 

##### Windows
Download [GTK+](https://sourceforge.net/projects/gladewin32/files/gtk%2B-win32-runtime/2.10.11/gtk-2.10.11-win32-1.exe/download?use_mirror=netactuate&modtime=1175123376&big_mirror=0"GTK+") installer.

##### Linux
Run in Terminal this command:
```terminal
$ sudo apt-get install libgtk2.0-dev
$ sudo apt-get install build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev
```

##### macOS
1. In conomand Line
```terminal
$ sudo xcode-select --install
$ sudo xcodebuild -license
```
2. Install [XQuartz](https://www.xquartz.org/"XQuartz 2.8.1") project.
3. Download the [Gtk-sh](https://gitlab.gnome.org/GNOME/gtk-osx/-/blob/master/gtk-osx-setup.sh"Gtk-osx-setup.sh") file and then run it with the following command:
```terminal
$ cd ~/Downloads
$ sh gtk-osx-setup.sh
```
4. Download the [MacPorts Extension](https://www.macports.org/install.php"MacPorts") according to system requirements

#### RQDA instalation

After the command at the beginning of the 2.4 RQDA installer, type the following in the console.
```R
install.packages("RGtk2")
library(RGtk2)
pkgs <- c("RSQLite", "gWidgets2RGtk2", "DBI", "stringi", "igraph", "gWidgets2", "memoise", "digest") 
install.packages(pkgs)
#
# all pop-up boxes are accepted
devtools::install_github("RQDA/RQDA", force=TRUE, INSTALL_opts="--no-multiarch")
#
# and finally
library(RGtk2)
library(RSQLite)
library(memoise)
library(digest)
library(gWidgets2)
library(gWidgets2RGtk2)
library(DBI)
library(stringi)
library(igraph)
library(RQDA)
#
# The workspace can also be saved, which will avoid having to call the libraries 
# each time it's started, allowing work to continue drom the last point.
save.image("\\home\\[directory]")
RQDA()
```
## 3. RQDA Tools

### 3.1 Project
An rqda file will be created containing all the recommended information, or a saved one will be opened. In this section are the following options:
1. **New Project**: Set a new directory to rqda project
2. **Open Project**: Open a rqda file
3. **Close Project**: Close the open project
4. **Project Memo**: Create a memo to project file
5. **Backup Project**: Create a backup so as not to lose information
6. **Save Project As**: Save project with other terms
7. **Clean Project**: Clean unused files
8. **Close all codings**: Close all open windows about codings



